//
//  ViewController.swift
//  MIS-System
//
//  Created by Martin Gonzalez on 25/10/18.
//  Copyright © 2018 Engrane. All rights reserved.
//

import UIKit


class TitleTableViewCell: UITableViewCell {
  
    @IBOutlet weak var img_title: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    
}

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tv: UITableView!

    var nombres: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
         nombres = ["Videos","Documentos","Enlaces web"]
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nombres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "title_cell", for: indexPath) as! TitleTableViewCell
        cell.lbl_title.text = nombres[indexPath.row]
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //performSegue(withIdentifier: "segue_form", sender: nil)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    


}

